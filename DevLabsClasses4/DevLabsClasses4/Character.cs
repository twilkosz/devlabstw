﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses4
{
    abstract class Character
    {
        protected int health;
        protected int strenght;
        public string name {get; private set; }

        public Character(int health, int strenght, string name)
        {
            this.health = health;
            this.strenght = strenght;
            this.name = name;
        }

        protected abstract int CalculateAttackPower();

        public int Substrakt(int value)
        {
            health -= value;
            return health > 0 ? health : 0;
        }

        public override string ToString()
        {
            return $"Name {name}, helath {health}, attack power {CalculateAttackPower()}";
         //   return "Name: " + name + " Health: " + health + " Attack power: " + CalculateAttackPower();
        }
    }
}
