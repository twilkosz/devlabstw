﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses4.Company
{
    class FullTimeContract : IContract
    {
        public FullTimeContract(decimal monthlyRate = 5000, uint overHours = 0)
        {
            this.MonthlyRate = monthlyRate;
            this.OverTimeHours = overHours;
        }
        private decimal _monthlyRate;
        private decimal MonthlyRate
        {
            get
            {
                return _monthlyRate;
            }
            set
            {
                if (value == 0 || value < 0)
                    Console.WriteLine("Incorrect value");
                else
                    _monthlyRate = value;
            }
        }
        private uint OverTimeHours;

        public decimal Salary()
        {
            return OverTimeHours == 0 ? MonthlyRate : MonthlyRate + (OverTimeHours * MonthlyRate / 60);
        }
    }
}
