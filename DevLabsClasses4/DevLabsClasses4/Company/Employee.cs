﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses4.Company
{
    class Employee
    {
        string name { get; set; }
        string surname { get; set; }
        IContract contractType { get; set; }

        public Employee(string name, string surname)
        {
            this.contractType = new TraineeContract();
            this.name = name;
            this.surname = surname;
        }

        public void ChangeContract(IContract contract)
        {
            this.contractType = contract;
        }


        public override string ToString()
        {
            return $"{name} {surname} {contractType.Salary()}";
        }
    }


}
