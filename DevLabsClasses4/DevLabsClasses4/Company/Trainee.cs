﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses4.Company
{
    class TraineeContract : IContract
    {
        public TraineeContract(decimal monthlyRate = 1000)
        {
            this.MonthlyRate = monthlyRate;
        }
        private decimal _monthlyRate;
        private decimal MonthlyRate
        {
            get
            {
                return _monthlyRate;
            }
            set
            {
                if (value == 0 || value < 0)
                    Console.WriteLine("Incorrect value");
                else
                    _monthlyRate = value;
            }
        }

        public decimal Salary()
        {
            return MonthlyRate;
        }
    }
}
