﻿using DevLabsClasses4.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Character wiz = new Wizard(100, 10, "Miroslaw", 15);
            //Character war = new Warrior();
            //Character wiz2 = new Wizard();
            //Console.WriteLine(wiz);
            //Console.WriteLine(war);
            //Console.WriteLine(wiz2);


            //Team Ateam = new Team("Leniwa");
            //Ateam.AddTeamMember(wiz);
            //Ateam.AddTeamMember(wiz2);

            //Character test = Ateam["Miroslaw"];


            Employee a1 = new Employee("Jan", "Motyka");
            Employee a2 = new Employee("Jan", "Kwas");

            IContract con1 = new FullTimeContract(5000, 10);
            IContract con2 = new FullTimeContract();


            Console.WriteLine(a1.ToString());
            a1.ChangeContract(con1);
            a2.ChangeContract(con2);
            Console.WriteLine(a1.ToString());
            Console.WriteLine(a2.ToString());

            Console.ReadKey();
        }
    }
}
