﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses4
{
    class Wizard : Character
    {
        private int magicPoints;

        public Wizard(int health = 100, int strenght = 10, string name = "Anonymous Mage", int magicPoints = 10) : base(health, strenght, name)
        {
            this.magicPoints = magicPoints;
        }

        protected override int CalculateAttackPower()
        {
            return (magicPoints + strenght) * health;
        }
    }
}
