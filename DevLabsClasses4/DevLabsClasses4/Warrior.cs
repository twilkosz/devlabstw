﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses4
{
    class Warrior : Character
    {
        public Warrior(int health = 100, int strenght = 20, string name = "Anonymous Warrior") : base(health, strenght, name)
        {
        }

        protected override int CalculateAttackPower()
        {
            return health < 5 ? strenght * 100 : strenght * health;
        }
    }
}
