﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses4
{
    class Team 
    {
        private List<Character> teamMembers;
        private string teamName;

        public Team(string name)
        {
            teamName = name;
            teamMembers = new List<Character>();
        }

        public void AddTeamMember(Character member)
        {
            teamMembers.Add(member);
        }

        public Character this[string name]
        {
            get
            {
                if (teamMembers != null && teamMembers.Count == 0)
                    return null;
                return teamMembers.Find(x => x.name == name);
            }
        }
    }
}
