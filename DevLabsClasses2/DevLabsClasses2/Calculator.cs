﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DevLabsClasses2
{
    class Calculator
    {
        public int StartCalculation()
        {
            string inputFromUser;
            bool validationResult;
            Stack<object> StackFromUser = new Stack<object>();
            int result = 0;
            inputFromUser = TakeInput();
            validationResult = ValidateInput(inputFromUser);


            if (validationResult)
            {
                StackFromUser = TranslateToONP(inputFromUser);
                result = CalculateONP(StackFromUser);
            }
            return result;
        }

        private string TakeInput()
        {
            char stopChar = '.';
            string output = "", input;

            while (stopChar != '=')
            {
                input = Console.ReadLine();
                if (input[input.Length - 1] == '=')
                {
                    input = input.Remove(input.Length - 1);
                    stopChar = '=';
                }
                output += input;
            }
            output = output.Replace(" ", "");
            return output;
        }

        private bool ValidateInput(string s)
        {
            char previous = 'a';
            int openBracket = 0;
            int closeBracket = 0;
            bool isCorrect = true;
            foreach (char c in s)
            {
                if (!char.IsNumber(c) && previous == 'a' && !isBracket(c))
                {
                    Console.WriteLine("Rownanie nie moze zaczynać się od symbolu!");
                    isCorrect = false;
                }

                if (!char.IsNumber(c) && !char.IsNumber(previous) && !isBracket(previous) && previous != 'a' && !isBracket(c))
                {
                    Console.WriteLine("Dwa znaki po sobie też nie są dozwolone");
                    isCorrect = false;
                }

                if (char.IsLetter(c))
                {
                    Console.WriteLine("Ta wersja kalkulatora nie obsluguje liczb");
                    isCorrect = false;
                }

                if (isBracket(c))
                {
                    if (c == '(')
                        openBracket++;
                    else
                        closeBracket++;

                    isCorrect = openBracket >= closeBracket;
                }
                previous = c;
            }

            return isCorrect = openBracket == closeBracket;
        }

        private bool isBracket(char c)
        {
            if (c == '(' || c == ')')
                return true;
            else
                return false;
        }
        private Stack<object> TranslateToONP(string eq)
        {
            Stack<char> WorkStack = new Stack<char>();
            Stack<object> FinalStack = new Stack<object>();
            string resultString;
            int newCharPrio = 0;
            int stackCharPrio = 0;
            Dictionary<char, int> PrioTab = new Dictionary<char, int>()
            {
                { '(', 0 },
                { '+', 1 },
                { '-', 1 },
                { ')', 1 },
                { '*', 2 },
                { '/', 2 },
                { '%', 2 },
                { '^', 3 }

            };

            for (int i = 0; i < eq.Length; i++)
            {
                if (Char.IsNumber(eq[i]))
                {
                    //If number occurs in string take whole number and display it
                    resultString = Regex.Match(eq.Substring(i), @"\d+").Value;
                    if (!string.IsNullOrEmpty(resultString))
                        i += resultString.Length - 1;

                    Console.Write(resultString + " ");
                    FinalStack.Push(resultString);
                }
                else
                {
                    if (eq[i] == ')')
                    {
                        while (WorkStack.Peek() != '(')
                        {
                            FinalStack.Push(WorkStack.Peek());
                            Console.Write(WorkStack.Pop() + " ");
                        }
                        WorkStack.Pop();
                    }
                    else
                    {
                        //Prepare prios to compare, watch out stock may be empty
                        PrioTab.TryGetValue((char)eq[i], out newCharPrio);
                        if (WorkStack.Count > 0)
                            PrioTab.TryGetValue((char)WorkStack.Peek(), out stackCharPrio);
                        //If loaded operator is higher then operator on top just add it to stock else take all with higher value
                        if (newCharPrio > stackCharPrio || eq[i] == '(')
                        {
                            WorkStack.Push(eq[i]);
                        }
                        else
                        {
                            while (stackCharPrio > newCharPrio)
                            {
                                if (WorkStack.Count() == 0)
                                    break;
                                FinalStack.Push(WorkStack.Peek());
                                Console.Write(WorkStack.Pop() + " ");
                                if (WorkStack.Count > 0)
                                    PrioTab.TryGetValue((char)WorkStack.Peek(), out stackCharPrio);
                            }
                            WorkStack.Push(eq[i]);
                        }

                    }
                }
            }
            //Clear stock
            while (WorkStack.Count != 0)
            {
                FinalStack.Push(WorkStack.Peek());
                Console.Write(WorkStack.Pop() + " ");
            }
            return FinalStack;
        }


        private int CalculateONP(Stack<object> stackToCalculate)
        {
            Stack<int> Calculate = new Stack<int>();
            int a, b, result = 0;

            stackToCalculate = new Stack<object>(stackToCalculate);
            while (stackToCalculate.Count != 0)
            {
                string takenFromStock = stackToCalculate.Pop().ToString();

                if (char.IsDigit(takenFromStock[0]))
                {
                    Calculate.Push(int.Parse(takenFromStock));
                }
                else
                {
                    a = Calculate.Pop();
                    b = Calculate.Pop();
                    switch (takenFromStock[0])
                    {
                        case '+':
                            result = b + a;
                            break;
                        case '-':
                            result = b - a;
                            break;
                        case '*':
                            result = b * a;
                            break;
                        case '/':
                            result = b / a;
                            break;

                    }
                    Calculate.Push(result);
                }
            }
            return Calculate.Pop();
        }
    }
}

