﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses2
{
    class ListZipper
    {
        public List<int> SumElements(List<int> ListOne, List<int> ListTwo)
        {
            List<int> WorkList = new List<int>();
                int size = ListOne.Count > ListTwo.Count ? ListOne.Count : ListTwo.Count;

            for (int i = 0; i < size; i++)
            {
                WorkList.Add(ListOne.ElementAtOrDefault(i) + ListTwo.ElementAtOrDefault(i));
            }
            return WorkList;
        }

        public List<int> GlueInTurns(List<int> ListOne, List<int> ListTwo)
        {
            int size = ListOne.Count > ListTwo.Count ? ListTwo.Count : ListOne.Count;
            List<int> WorkList = new List<int>();

            for (int i = 0; i < size; i++)
            {
                WorkList.Add(ListOne.ElementAtOrDefault(i));
                WorkList.Add(ListTwo.ElementAtOrDefault(i));
            }

            return WorkList;
        }

    }
}
