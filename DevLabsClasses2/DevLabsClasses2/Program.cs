﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses2
{
    class Program
    {
        static void Main(string[] args)
        {
            //   string sentecne = "Ale kupila kotkowi karme dla kota ma nadzieje ze dzieki temu jej kot bedzie sie cieszysl kot kot kot";

            //   string word = "kot";
            //   string word2 = "pies";

            //   string result = FindAndReplace(sentecne, word, word2); // CheckIfFoundWordIsTheSame(sentecne, 7, word);
            //   Console.WriteLine(result);


            // List<int> a = new List<int>() { 1, 2, 5, 6 };
            //  List<int> b = new List<int>() { 9, 0 };
            // ListZipper lisZip = new ListZipper();

            //  List<int> c = lisZip.SumElements(a, b);
            //   List<int> d = lisZip.GlueInTurns(a, b);

            Calculator calculator = new Calculator();

            string equation = "12+5*(2*1+18/123)";     // EXPECTED: 12 5 2 1 * 18 123 / + * +
            string equation2 = "((2+7)/3+(14-3)*4)/2"; // EXPECTED: 2 7 + 3 / 14 3 - 4 * + 2 /
            string equation3 = "((15/(7-(1+1)))*3)-(2+(1+1))"; // EXPECTED: 15 7 1 1 + − / 3 * 2 1 1 + + −


            int result= calculator.StartCalculation();
            Console.WriteLine(result);
            Console.ReadKey();
        }
    }

}