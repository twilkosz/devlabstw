﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses2
{
    class StringBuilder
    {
        public string FindAndReplace(string s, string oldWord, string newWord)
        {
            char[] workCharTable = s.ToCharArray();
            bool result = false;
            for (int i = 0; i < workCharTable.Length; i++)
            {
                if (workCharTable[i] == oldWord[0])
                {
                    result = CheckIfFoundWordIsTheSame(workCharTable, i, oldWord);
                    if (result == true) // && s.Length > i + w.Length && s[i + w.Length] != ' '
                    {
                        workCharTable = ReplaceinTable(workCharTable, oldWord, newWord, i);
                        result = false;
                    }
                }
            }
            return new string(workCharTable);
        }


        private bool CheckIfFoundWordIsTheSame(char[] sentence, int position, string word)
        {
            bool result = true;
            for (int i = 0; i < word.Length; i++)
            {
                if (sentence.Length == i + position)
                {
                    result = false;
                    break;
                }
                else if (sentence[i + position] != word[i])
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        private char[] ReplaceinTable(char[] charTable, string oldWord, string newWord, int position)
        {
            int resizeValue = newWord.Length - oldWord.Length;
            char[] newTable = new char[charTable.Length + resizeValue];
            bool replaced = false;
            for (int y = 0; y <= charTable.Length; y++)
            {
                if (y == position)
                {
                    for (int i = 0; i < newWord.Length; i++)
                    {
                        newTable[y] = newWord[i];
                        y++;
                    }
                    replaced = true;
                }

                if (y == newTable.Length) //after addin word with different size we need to check if we are not going to extend table
                    break;

                if (replaced)
                    newTable[y] = charTable[y - resizeValue];
                else
                    newTable[y] = charTable[y];
            }
            return newTable;
        }
    }
}
