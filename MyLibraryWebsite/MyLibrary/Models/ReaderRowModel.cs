﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyLibrary.Models
{
    public class ReaderRowModel
    {
        public string FirstName { get; set; }
        public  string LastName { get; set; }
        public bool Blocked { get; set; }
    }
}