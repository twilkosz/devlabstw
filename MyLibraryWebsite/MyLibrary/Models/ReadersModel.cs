﻿using System.Collections.Generic;


namespace MyLibrary.Models
{
    public class ReadersModel
    {
       public IEnumerable<ReaderRowModel> ReaderRows { get; set; }
    }
}