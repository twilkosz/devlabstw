﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyLibrary.Models
{
    public class BookBorrowingModel
    {
        public IEnumerable<BookBorrowingRowModel> BorrowingRows { get; set; }
    }
}