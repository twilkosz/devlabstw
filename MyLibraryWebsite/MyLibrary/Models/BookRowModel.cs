﻿namespace MyLibrary.Models
{
    public class BookRowModel
    {
        public string Title { get; set; }

        public string Author { get; set; }

        public int PublishingYer { get; set; }

        public int Pages { get; set; }
    }
}