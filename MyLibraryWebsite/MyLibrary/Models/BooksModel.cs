﻿using System.Collections.Generic;

namespace MyLibrary.Models
{
    public class BooksModel
    {
        public IEnumerable<BookRowModel> BookRows { get; set; }
    }
}