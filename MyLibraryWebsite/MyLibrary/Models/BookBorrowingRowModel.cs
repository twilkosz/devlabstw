﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyLibrary.Models
{
    public class BookBorrowingRowModel
    {
        public string ReaderFirstName { get; set; }
        public string ReaderLastName { get; set; }
        public string BookTitle { get; set; }
        public string BorrowingDate { get; set; }
        public string ReturnDate { get; set; }


    }
}