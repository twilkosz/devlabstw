﻿using MyLibrary.Models;
using MyLibrary.Models.Models;
using MyLibrary.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Volvo.NVS.Persistence.NHibernate.Web.SessionHandling;
using System;

namespace MyLibrary.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [NHibernateMvcSessionContext()]
        public ActionResult Books()
        {
            var bookService = new BookService();
            var books = bookService.GetAllBooks();
            var model = TranslateBooksToPresentationModel(books);

            return View(model);
        }

        [NHibernateMvcSessionContext()]
        public ActionResult BooksNotReturned()
        {
            var bookService = new BookService();
            var books = bookService.BooksNotReturned();
            var model = TranslateBooksToPresentationModel(books);

            return View(model);
        }

        private BooksModel TranslateBooksToPresentationModel(IEnumerable<Book> books)
        {
            var model = new BooksModel();
            model.BookRows = books.Select(b => new BookRowModel
            {
                Author = string.Join(", ", b.Title.Authors.Select(a => a.Name)),
                Title = b.Title.Title,
                Pages = b.Pages,
                PublishingYer = b.PublishingYear
            });
            return model;
        }

        [NHibernateMvcSessionContext()]
        public ActionResult Readers()
        {
            var readerService = new ReaderService();
            var readers = readerService.GetAllReaders();
            

            var model = TranslateReadersToPresentationModel(readers);

            return View(model);
        }

        private ReadersModel TranslateReadersToPresentationModel(IEnumerable<Reader> readers)
        {
            var model = new ReadersModel();
            model.ReaderRows = readers.Select(b => new ReaderRowModel
            {
                FirstName = b.FirstName,
                LastName = b.LastName,
                Blocked=b.Blocked
            });
            return model;
        }

        [NHibernateMvcSessionContext()]
        public ActionResult BookBorrowing()
        {
            var borrowingService = new BookBorrowingService();
            var borrowings = borrowingService.GetAllBorrowings();

            var model = TranslateBorrowingsToPresentationModel(borrowings);
            return View(model);
        }

        private BookBorrowingModel TranslateBorrowingsToPresentationModel(IEnumerable<BookBorrowing> borrowings)
        {
            var model = new BookBorrowingModel();

            model.BorrowingRows = borrowings.Select(b => new BookBorrowingRowModel
            {
                BookTitle=b.book.Title.Title,
                ReaderFirstName=b.reader.FirstName,
                ReaderLastName=b.reader.LastName,
                BorrowingDate=b.borrowingDate,
                ReturnDate=b.returnDate
            }
            );

            return model;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}