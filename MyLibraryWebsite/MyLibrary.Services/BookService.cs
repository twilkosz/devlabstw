﻿using MyLibrary.Models.Models;
using MyLibrary.Repository.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace MyLibrary.Services
{
    public class BookService
    {
        private readonly BookRepository _bookRepo;
        public BookService()
        {
            _bookRepo = new BookRepository();
        }

        public IList<Book> GetAllBooks()
        {
            return _bookRepo.FindAll();
        }

        public IList<Book> BooksNotReturned()
        {
            IList<Book> books;
            BookBorrowingService bookBorrowingService = new BookBorrowingService();

            var borrowings = bookBorrowingService.GetNotRetrned();

            books = borrowings.Select(x => x.book).ToList<Book>();

            return books;
        }
    }
}
