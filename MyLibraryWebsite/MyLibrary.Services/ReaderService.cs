﻿using MyLibrary.Models.Models;
using MyLibrary.Repository.Repositories;
using MyLibrary.Services.Specifications;
using System.Collections.Generic;


namespace MyLibrary.Services
{
    public class ReaderService
    {
        private readonly ReaderRepository _readerRepo;
        public ReaderService()
        {
            _readerRepo = new ReaderRepository();
        }

        public IList<Reader> GetAllReaders()
        {
            return _readerRepo.FindAll();
        }

        public IList<Reader> GetAllReadersStartingWIthLetter(char c)
        {
            return _readerRepo.Find(new ReaderByName(c));
        }

        //   public IList<Book> BooksNotReturned()
        //   {   //use repository and specification here to find all books that have latest book borrowing with null return date
        //return null;
        //    }
    }
}
