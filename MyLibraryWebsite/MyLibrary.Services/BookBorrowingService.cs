﻿using MyLibrary.Models.Models;
using MyLibrary.Repository.Repositories;
using MyLibrary.Services.Specifications;
using System.Collections.Generic;

namespace MyLibrary.Services
{
    public class BookBorrowingService
    {
        private readonly BookBorrowingRepository _bookBorrowingRepo;
        public BookBorrowingService()
        {
            _bookBorrowingRepo = new BookBorrowingRepository();
        }

        public IList<BookBorrowing> GetAllBorrowings()
        {
            return _bookBorrowingRepo.FindAll();
        }

        public IList<BookBorrowing> GetNotRetrned()
        {
            return _bookBorrowingRepo.Find(new BorrowingsNotReturned());
        }
    }
}
