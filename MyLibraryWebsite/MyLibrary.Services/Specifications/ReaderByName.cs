﻿using MyLibrary.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volvo.NVS.Persistence.Specifications;

namespace MyLibrary.Services.Specifications
{
    public class ReaderByName: Specification<Reader>
    {

        public ReaderByName(char c) : base(x => x.FirstName.Substring(0,1) == c.ToString()) { }
    }
}
