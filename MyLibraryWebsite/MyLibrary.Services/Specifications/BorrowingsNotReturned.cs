﻿using MyLibrary.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volvo.NVS.Persistence.Specifications;

namespace MyLibrary.Services.Specifications
{
    public class BorrowingsNotReturned:Specification<BookBorrowing>
    {
        public BorrowingsNotReturned() : base(x => x.returnDate == null) { }
    }
}
