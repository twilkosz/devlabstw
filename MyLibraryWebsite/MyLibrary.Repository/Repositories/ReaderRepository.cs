﻿using MyLibrary.Models.Models;
using Volvo.NVS.Persistence.NHibernate.Repositories;

namespace MyLibrary.Repository.Repositories
{
    public class ReaderRepository : GenericRepository<Reader, int>
    {
    }
}
