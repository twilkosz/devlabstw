﻿using MyLibrary.Models.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MyLibrary.Repository.Mappings
{
    public class ReaderMap : ClassMapping<Reader>
    {
        public ReaderMap()
        {
            Schema("[dbo]");
            Table("Reader");
            Id(x => x.Id, map => map.Generator(Generators.Native));

            Property(p => p.FirstName, p => p.NotNullable(true));
            Property(p => p.LastName, p => p.NotNullable(true));
            Property(p => p.BirthDate, p => p.NotNullable(true));
            Property(p => p.EnrollDate, p => p.NotNullable(true));
            Property(p => p.Pesel, p => p.NotNullable(true));
            Property(p => p.Gender, p => p.NotNullable(true));
            Property(p => p.Blocked, p => p.NotNullable(true));
        }
    }
}
