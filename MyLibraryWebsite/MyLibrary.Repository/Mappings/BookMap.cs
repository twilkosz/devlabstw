﻿using MyLibrary.Models.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MyLibrary.Repository.Mappings
{
    public class BookMap : ClassMapping<Book>
    {
        public BookMap()
        {
            Schema("[dbo]");
            Table("Book");
            Id(x => x.Id, map => map.Generator(Generators.Native));

            Property(p => p.ISBN, p => p.NotNullable(true));
            Property(p => p.PublishingYear, p => p.NotNullable(true));
            Property(p => p.Hardcover, p => p.NotNullable(true));
            Property(p => p.Pages, p => {p.NotNullable(true);});

            ManyToOne<BookTitle>(b => b.Title, b => 
            {
                b.Cascade(Cascade.None);
                b.Column("TitleId");
                b.NotNullable(true);
                b.Lazy(LazyRelation.NoLazy);
            });
        }
    }
}
