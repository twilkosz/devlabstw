﻿using MyLibrary.Models.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MyLibrary.Repository.Mappings
{
    class BookBorrowingMap : ClassMapping<BookBorrowing>
    {
        public BookBorrowingMap()
        {
            Schema("[dbo]");
            Table("BookBorrowing");

            Id(x => x.Id, map => map.Generator(Generators.Native));

            Property(p => p.returnDate, p => p.NotNullable(true));
            Property(p => p.borrowingDate, p => p.NotNullable(true));

            ManyToOne<Book>(b => b.book, b =>
            {
                b.Cascade(Cascade.None);
                b.Column("BookId");
                b.NotNullable(true);
                b.Lazy(LazyRelation.NoLazy);
            });

            ManyToOne<Reader>(b => b.reader, b =>
            {
                b.Cascade(Cascade.None);
                b.Column("ReaderId");
                b.NotNullable(true);
                b.Lazy(LazyRelation.NoLazy);
            });
        }
    }
}
