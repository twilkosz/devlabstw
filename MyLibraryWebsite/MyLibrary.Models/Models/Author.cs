﻿using Volvo.NVS.Persistence.NHibernate.Entities;

namespace MyLibrary.Models.Models
{
    public class Author : GenericEntity<int>
    {
        public virtual string Name { get; set; }
    }
}
