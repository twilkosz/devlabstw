﻿using System.Collections.Generic;
using Volvo.NVS.Persistence.NHibernate.Entities;

namespace MyLibrary.Models.Models
{
    public class BookTitle : GenericEntity<int>
    {
        public virtual string Title { get; set; }

        public virtual IEnumerable<Author> Authors { get; set; }
    }
}
