﻿using System.Collections.Generic;
using Volvo.NVS.Persistence.NHibernate.Entities;

namespace MyLibrary.Models.Models
{
    public class Reader : GenericEntity<int>
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string BirthDate { get; set; }
        public virtual string EnrollDate { get; set; }
        public virtual char Gender { get; set; }
        public virtual string Pesel { get; set; }
        public virtual bool Blocked { get; set; }
    }
}
