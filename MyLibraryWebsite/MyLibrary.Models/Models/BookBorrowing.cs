﻿using System.Collections.Generic;
using Volvo.NVS.Persistence.NHibernate.Entities;

namespace MyLibrary.Models.Models
{
    public class BookBorrowing : GenericEntity<int>
    {
        public virtual Book book { get; set; }
        public virtual Reader reader { get; set; }

        public virtual string borrowingDate { get; set; }
        public virtual string returnDate { get; set; }
    }
}
