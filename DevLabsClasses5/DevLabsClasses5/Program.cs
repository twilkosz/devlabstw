﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses5
{
    class Program
    {
        static void Main(string[] args)
        {
            ComplexNumber a;
            ComplexNumber b;
            ComplexNumber c;
            ComplexNumber d;
            a = new ComplexNumber(1,-2);
            b = new ComplexNumber(1, -2);


            // Console.WriteLine(b);
            // Console.WriteLine(c);
             Console.WriteLine(a*b);
            DaysInMonth(DateTime.Now);
            Console.ReadKey();
        }

        public static void DayProperties(DateTime date)
        {
            Console.WriteLine("Complex type: " + date.ToString("g", new CultureInfo("en-US")));
            Console.WriteLine("Short date: " + date.ToString("d", new CultureInfo("en-US")));
            Console.WriteLine("Display date using 24 H format:");
            Console.WriteLine(date.ToString("d", new CultureInfo("en-US")) + "   " + date.ToString("t", new CultureInfo("en-US")));
            Console.WriteLine(date.ToString("MM/dd/yyyy HH:MM", new CultureInfo("en-US"))); // with slashes 
            Console.WriteLine("{0:MM/dd/yyyy HH:MM}", date); // with dash
        }

        public static void DayProperties2(DateTime date)
        {
            Console.WriteLine($"Year = {date.Year} \nMonth = {date.Month} \nDay = {date.Day} \nHour = {date.Hour} \nMinute = {date.Minute} \nSecond = {date.Second} \nMillisecond = {date.Millisecond}");
        }

        public static void DayOfWeek(DateTime date)
        {
            Console.WriteLine($"The day of the week for {date.ToShortDateString()} is {date.DayOfWeek}");
        }

        public static void NumberOfDays(int year)
        {
            if (DateTime.IsLeapYear(year))
                Console.WriteLine($"{year}: has 366 days. Is a Leap year!");
            else
                Console.WriteLine($"{year}: has 365 days.");
        }
        public static void NumberofDaysWithRange(int startYear, int endYear)
        {
            while (startYear != endYear + 1)
            {
                NumberOfDays(startYear);
                startYear++;
            }
        }

        public static void CurrentDate(DateTime date)
        {
            Console.WriteLine("Local Time: " + date.ToString());
            Console.WriteLine("UTC Time: " + date.ToUniversalTime().ToString());
        }

        public static void XdaysFromNow(int days)
        {
            DateTime date = DateTime.Now;
            Console.WriteLine($"Start date = {date.ToString()}");
            date = date.AddDays(days);
            Console.WriteLine($"{days} from now is {date.DayOfWeek}");
        }

        public static void MonthsAroundNow(DateTime date, int range)
        {

            for (int i = -1 * range; i <= range; i++)
            {
                if (i == 0)
                    Console.WriteLine(DateTime.Now.AddMonths(i).ToShortDateString() + " (start date)");
                else
                    Console.WriteLine(DateTime.Now.AddMonths(i).ToShortDateString());
            }
        }

        public static void GetNextWeekday(DateTime date, DayOfWeek day)
        {

            int dayDif = ((int)day - (int)date.DayOfWeek + 7) % 7;
            Console.WriteLine(date.AddDays(dayDif));
        }

        public static void CompareDates(DateTime a, DateTime b)
        {
            if (a > b)
                Console.WriteLine($"{a} is greater than {b}");
            else
                Console.WriteLine($"{b} is greater than {a}");
        }

        public static void DaysInMonth(DateTime date)
        {
            Console.WriteLine($"For {(Month)date.Month} {date.Year}: {DateTime.DaysInMonth(date.Year, date.Month)}");
        }

        public enum Month
        {
            NotSet = 0,
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12
        }
        struct ComplexNumber
        {
            public double real { get; private set; }
            public double imaginary { get; private set; }

            public ComplexNumber(double real = 0, double imaginary = 0)
            {
                this.real = real;
                this.imaginary = imaginary;
            }

            public override string ToString()
            {
                if (real == 0)
                {
                    return $"{imaginary}i";
                }
                else
                    return imaginary >= 0 ? $"{real}+{imaginary}i" : $"{real}{imaginary}i";
            }

            public static ComplexNumber operator +(ComplexNumber a, ComplexNumber b)
            {
                ComplexNumber comp = new ComplexNumber();
                comp.real = a.real + b.real;
                comp.imaginary = a.imaginary + b.imaginary;
                return comp;
            }

            public static ComplexNumber operator -(ComplexNumber a, ComplexNumber b)
            {
                ComplexNumber comp = new ComplexNumber();
                comp.real = a.real - b.real;
                comp.imaginary = b.imaginary - b.imaginary;
                return comp;
            }

            public static ComplexNumber operator *(ComplexNumber a, ComplexNumber b)
            {
                ComplexNumber comp = new ComplexNumber();
                comp.real = (a.real * b.real) - (a.imaginary * b.imaginary);
                comp.imaginary = (b.imaginary * a.real) + (a.imaginary * b.real);
                return comp;
            }

            public static ComplexNumber operator /(ComplexNumber a, ComplexNumber b)
            {
                ComplexNumber comp = new ComplexNumber();
                comp.real = ((a.real * b.real) + (a.imaginary * b.imaginary)) / (b.real * b.real + b.imaginary * b.imaginary);
                comp.imaginary = ((b.imaginary * a.real - (a.imaginary * b.real))) / (b.real * b.real + b.imaginary * b.imaginary);
                return comp;
            }
        }
    }
}
