﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses3
{
    interface IPriorityCollection
    {
        void Insert(int priority, object value);
        object Pop(); 
    }
}
