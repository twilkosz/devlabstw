﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses3
{
    class BinarySearchTree
    {
        private Node rootNode;
        private List<Node> AllNodes;
        private StringBuilder sb;

        public BinarySearchTree()
        {
            AllNodes = new List<Node>();
            sb = new StringBuilder();
        }
        public void Insert(int key, object value)
        {
            Node nodeToInsert = new Node(key, value);
            AllNodes.Add(nodeToInsert);

            rootNode = InsertToNode(nodeToInsert, rootNode);

        }
        public override string ToString()
        {
            List<Node> sortedList = AllNodes.OrderBy(o => o.key).ToList();
            sb.Append("{");
            foreach (var element in sortedList)
            {
                sb.Append("[").Append(element.key).Append(",").Append(element.value.ToString()).Append("]").Append(", ");
            }
            if(sb.Length!=1)
            sb.Remove(sb.Length - 2, 2);

            sb.Append("}");
            return sb.ToString();
        }

        private Node InsertToNode(Node nodeToInsert, Node existingOne)
        {
            if (existingOne == null)
            {
                existingOne = nodeToInsert;
            }
            else
            {
                if (existingOne.key > nodeToInsert.key)
                {
                    existingOne.LeftChild = InsertToNode(nodeToInsert, existingOne.LeftChild);
                }
                else
                {
                    existingOne.RightChild = InsertToNode(nodeToInsert, existingOne.RightChild);
                }
            }
            return existingOne;
        }

        public int Find(int key)
        {
            Node exactNode = FindExactNode(key, rootNode);
            return exactNode == null ? 0 : exactNode.key;
        }

        public int MinKey()
        {
            Node smallestNode = FindSmallestNode(rootNode);
            return smallestNode == null ? 0 : smallestNode.key;
        }

        private Node FindExactNode(int key, Node currentNode)
        {
            Node nodeToReturn = null;

            if (currentNode.key == key)
            {
                nodeToReturn = currentNode;
            }
            else if (currentNode.key < key && currentNode.RightChild != null)
            {
                nodeToReturn = FindExactNode(key, currentNode.RightChild);
            }
            else if (currentNode.key > key && currentNode.LeftChild != null)
            {
                nodeToReturn = FindExactNode(key, currentNode.LeftChild);
            }
            else if (currentNode.LeftChild == null && currentNode.RightChild == null)
            {
                nodeToReturn = null;
            }
            return nodeToReturn;
        }

        private Node FindSmallestNode(Node currentNode)
        {
            Node nodeToReturn;

            if (currentNode.LeftChild != null)
            {
                nodeToReturn = FindSmallestNode(currentNode.LeftChild);
            }
            else
                nodeToReturn = currentNode;
            return nodeToReturn;
        }

        public void Pop(int key)
        {
            Node nodeToPop = null, nodeParent;

            nodeToPop = FindExactNode(key, rootNode);

            if (nodeToPop != null)
            {
                nodeParent = FindParentNode(key, rootNode);
                if (nodeParent.LeftChild?.key == key)
                {
                    if (nodeToPop.LeftChild == null && nodeToPop.RightChild == null)
                        nodeParent.LeftChild = null;
                    else if (nodeToPop.LeftChild == null)
                        nodeParent.LeftChild = nodeToPop.RightChild;
                    else if (nodeToPop.RightChild == null)
                        nodeParent.LeftChild = nodeToPop.LeftChild;
                    else if (nodeToPop.LeftChild != null && nodeToPop.RightChild != null)
                    {
                        nodeParent.LeftChild = nodeToPop.RightChild;
                        InsertToNode(nodeToPop.LeftChild, nodeToPop.RightChild);
                    }

                }
                else
                {
                    if (nodeToPop.LeftChild == null && nodeToPop.RightChild == null)
                        nodeParent.RightChild = null;
                    else if (nodeToPop.LeftChild == null)
                        nodeParent.RightChild = nodeToPop.RightChild;
                    else if (nodeToPop.RightChild == null)
                        nodeParent.RightChild = nodeToPop.LeftChild;
                    else if (nodeToPop.LeftChild != null && nodeToPop.RightChild != null)
                    {
                        nodeParent.RightChild = nodeToPop.RightChild;
                        InsertToNode(nodeToPop.LeftChild, nodeToPop.RightChild);
                    }

                }

                AllNodes.Remove(nodeToPop);
            }
        }

        private Node FindParentNode(int key, Node currentNode)
        {
            Node nodeToReturn = null;

            if (currentNode.key == key)
            {
                nodeToReturn = null;
            }
            else if (currentNode.key < key && currentNode.RightChild != null)
            {
                if (currentNode.RightChild.key == key)
                {
                    nodeToReturn = currentNode;
                }
                else
                {
                    nodeToReturn = FindParentNode(key, currentNode.RightChild);
                }
            }
            else if (currentNode.key > key && currentNode.LeftChild != null)
            {
                if (currentNode.LeftChild.key == key)
                {
                    nodeToReturn = currentNode;
                }
                else
                {
                    nodeToReturn = FindParentNode(key, currentNode.LeftChild);
                }
            }
            else if (currentNode.LeftChild == null && currentNode.RightChild == null)
            {
                nodeToReturn = null;
            }
            return nodeToReturn;
        }
    }
}
