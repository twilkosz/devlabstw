﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses3
{
    class PriorityQueue : IPriorityCollection
    {
        private List<KeyValuePair<int, object>> listOfObjects;
        public PriorityQueue()
        {
            listOfObjects = new List<KeyValuePair<int, object>>();
        }
        public void Insert(int priority, object value)
        {
            KeyValuePair<int, object> obj = new KeyValuePair<int, object>(priority, value);
            int i = listOfObjects.Count - 1;

            if (listOfObjects.Count == 0 || listOfObjects[i].Key < obj.Key)
            {
                listOfObjects.Add(obj);
                return;
            }
            else
            {
                for (; i >= 0; i--)
                {
                    if (listOfObjects[i].Key < obj.Key)
                    {
                        listOfObjects.Insert(i+1, obj);
                        return;
                    }
                }
            }
        }

        public object Pop()
        {
            object lastElement;
            if (listOfObjects.Count <= 1)
            {
                Console.WriteLine("List is empty or single element left");
                return lastElement = new object();
            }
            lastElement = listOfObjects[listOfObjects.Count - 1];
            listOfObjects.RemoveAt(listOfObjects.Count - 1);

            return lastElement;
        }


    }
}
