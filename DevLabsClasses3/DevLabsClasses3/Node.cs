﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLabsClasses3
{
    class Node
    {
        public Node(int key, object value)
        {
            this.key = key;
            this.value = value;
        }
        public int key { get; set; }
        public object value { get; set; }

        public Node LeftChild, RightChild;
    }
}
