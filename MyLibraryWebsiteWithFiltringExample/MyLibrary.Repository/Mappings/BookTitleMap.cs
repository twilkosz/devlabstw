﻿using MyLibrary.Models.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MyLibrary.Repository.Mappings
{
    public class BookTitleMap : ClassMapping<BookTitle>
    {
        public BookTitleMap()
        {
            Schema("[dbo]");
            Table("BookTitle");
            Id(x => x.Id, map => map.Generator(Generators.Native));

            Property(p => p.Title, p => p.NotNullable(true));

            Bag<Author>(b => b.Authors, collection =>
                {
                    collection.Table("BookAuthor");
                    collection.Key(k => k.Column("BookTitleId"));
                    collection.Lazy(CollectionLazy.NoLazy);
                },
                map => map.ManyToMany(a => a.Column("AuthorId")));
        }
    }
}
