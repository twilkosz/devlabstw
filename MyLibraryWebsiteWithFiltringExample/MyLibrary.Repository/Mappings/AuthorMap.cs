﻿using MyLibrary.Models.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MyLibrary.Repository.Mappings
{
    public class AuthorMap : ClassMapping<Author>
    {
        public AuthorMap()
        {
            Schema("[dbo]");
            Table("Author");
            Id(x => x.Id, map => map.Generator(Generators.Native));

            Property(p => p.Name, p => p.NotNullable(true));
        }
    }
}
