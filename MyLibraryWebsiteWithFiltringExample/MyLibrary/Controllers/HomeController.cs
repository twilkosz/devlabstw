﻿using MyLibrary.Models;
using MyLibrary.Models.Models;
using MyLibrary.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Volvo.NVS.Persistence.NHibernate.Web.SessionHandling;

namespace MyLibrary.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [NHibernateMvcSessionContext()]
        public ActionResult Books(BooksModel booksModel)
        {
            var bookService = new BookService();
            var a = this.Request.Params["Next"];
            IList<Book> books;

            if (booksModel.SearchParams == null || string.IsNullOrEmpty(booksModel.SearchParams.Title))
                books = bookService.GetAllBooks();
            else
                books = bookService.SearchBooksByTitle(booksModel.SearchParams.Title);


           
            TranslateBooksToPresentationModel(booksModel, books);

            booksModel.BookRows = booksModel.BookRows.Skip((booksModel.PageNumber - 1)*5).Take(5);

            return View(booksModel);
        }

        private void TranslateBooksToPresentationModel(BooksModel targetModel, IEnumerable<Book> books)
        {
            targetModel.BookRows = books.Select(b => new BookRowModel
            {
                Author = string.Join(", ", b.Title.Authors.Select(a => a.Name)),
                Title = b.Title.Title,
                Pages = b.Pages,
                PublishingYer = b.PublishingYear
            });
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}