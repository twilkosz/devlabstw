﻿using System.Collections.Generic;

namespace MyLibrary.Models
{
    public class BooksModel
    {
        public IEnumerable<BookRowModel> BookRows { get; set; }

        public SearchBookModel SearchParams { get; set; }

        public int PageNumber { get; set; } = 1;

        public bool Next { get; set; }
        public bool Prev { get; set; }
    }
}