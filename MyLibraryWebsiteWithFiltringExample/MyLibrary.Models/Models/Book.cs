﻿using System.Collections.Generic;
using Volvo.NVS.Persistence.NHibernate.Entities;

namespace MyLibrary.Models.Models
{
    public class Book : GenericEntity<int>
    {
        public virtual string ISBN { get; set; }

        public virtual int PublishingYear { get; set; }

        public virtual bool Hardcover { get; set; }

        public virtual int Pages { get; set; }

        public virtual BookTitle Title { get; set; }
    }
}
