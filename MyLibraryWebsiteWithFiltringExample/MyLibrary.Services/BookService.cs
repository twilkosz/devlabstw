﻿using MyLibrary.Models.Models;
using MyLibrary.Repository.Repositories;
using MyLibrary.Services.Specifications;
using System.Collections.Generic;

namespace MyLibrary.Services
{
    public class BookService
    {
        private readonly BookRepository _bookRepo;
        public BookService()
        {
            _bookRepo = new BookRepository();
        }

        public IList<Book> GetAllBooks()
        {
            return _bookRepo.FindAll();
        }

        public IList<Book> SearchBooksByTitle(string text)
        {
            return _bookRepo.Find(new BookTitleSpecification(text));
        }

        public IList<Book> BooksNotReturned()
        {   //use repository and specification here to find all books that have latest book borrowing with null return date
            return null;
        }
    }
}
