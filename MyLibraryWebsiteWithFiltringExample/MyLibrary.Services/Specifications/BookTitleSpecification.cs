﻿using MyLibrary.Models.Models;
using Volvo.NVS.Persistence.Specifications;

namespace MyLibrary.Services.Specifications
{
    public class BookTitleSpecification : Specification<Book>
    {
        public BookTitleSpecification(string text) 
            : base(b => b.Title.Title.ToLower().Contains(text.Trim().ToLower())) {}
    }
}
