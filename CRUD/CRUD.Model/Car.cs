﻿namespace CRUD.Model
{
    public class Car
    {
        public virtual int Id { get; set; }
        public virtual string ModelName { get; set; }
        public virtual Driver Owner { get; set; }
    }
}
