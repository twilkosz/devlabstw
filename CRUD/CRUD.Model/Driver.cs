﻿using System.Collections.Generic;

namespace CRUD.Model
{
    public class Driver
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }

        public virtual IList<Car> Cars { get; set; }

        public virtual void AddCar(Car car)
        {
            if (Cars == null)
                Cars = new List<Car>();

            car.Owner = this;
            Cars.Add(car);
        }
    }
}
