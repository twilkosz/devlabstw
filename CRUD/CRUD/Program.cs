﻿using CRUD.Model;
using CRUD.Repository;
using CRUD.Repository.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using Volvo.NVS.Persistence.NHibernate.SessionHandling;

namespace CRUD
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = new string[] { "Tomek", "Jacek", "Gosia", "Marcinn", "Agata", "Magda", "Franklin", "Weronika", "Maciek", "Grzegorz" };
            string[] surnames = new string[] { "Adamczyk", "Dudek", "Zając", "Wieczorek", "Jabłoński", "Król", "Majewski", "Olszewski", "Jaworski", "Wróbel" };
            string[] models = new string[] { "m", "a", "s", "g", "h", "u", "q", "d", "i", "o" };
            int key = 0, menu = 0, menu2 = 0;
            Driver result;
            string input, modelname;

            DriverRepository DriverRepo;
            CarRepository CarRepo;

            InitialRepositories(out DriverRepo, out CarRepo);


            // CreateTestData(1000, names, surnames,models, DriverRepo);
            var watch = System.Diagnostics.Stopwatch.StartNew();
            using (new NHibernateSessionContext())
            {
                //var query = from c in DriverRepo.FindAll()
                //            where c.Name=="Tomek" 
                //             select new
                //             {
                //                 c.Name,
                //                 c.Surname
                //             };

                var query = DriverRepo.Find(new DriverByName("Tomek"));
            }

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine(elapsedMs);
            Console.ReadKey();



            Console.WriteLine("Please pick one of options: \n1)Find Driver \n2)UpdateUser \n3)Add Car \n4)Assign Car \n5)Find all Cars by owner Id");

            while (key != 9)
            {
                input = Console.ReadLine();
                if (int.TryParse(input, out key) == true && key >= 1 && key <= 9)
                {
                    switch (key)
                    {
                        case 1:
                            menu = GetCorrectId("Please provide user Id");
                            result = GetDriverById(menu, DriverRepo);
                            if (result == null)
                                Console.WriteLine("No such id in database");
                            else
                                Console.WriteLine($"ID: {result.Id} Name: {result.Name} Surname: {result.Surname}");
                            break;

                        case 2:
                            menu = GetCorrectId("Please provide user Id");
                            result = GetDriverById(menu, DriverRepo);
                            if (result == null)
                                Console.WriteLine("No such id in database");
                            else
                            {
                                string newName, newSurname;
                                Console.WriteLine("Please provide new name");
                                newName = Console.ReadLine();
                                Console.WriteLine("Please provide new surname");
                                newSurname = Console.ReadLine();

                                UpdateDriverNameAndSurnameById(menu, newName, newSurname, DriverRepo);
                            }
                            break;

                        case 3:
                            Console.WriteLine("please write model name");
                            modelname = Console.ReadLine();
                            CreateCar(modelname, CarRepo);
                            break;

                        case 4:
                            menu = GetCorrectId("Please pick car");
                            menu2 = GetCorrectId("Please pick owner");

                            if (menu == 0 || menu2 == 0)
                            {
                                Console.WriteLine("Incorrect Input");
                            }
                            else
                                UpdateCar(menu, menu2, CarRepo);

                            break;

                        case 5:
                            List<Car> carList = new List<Car>();
                            menu = GetCorrectId("Please provide owner Id");
                            carList = FindDriversCar(menu, CarRepo);
                            DisplayCarList(carList);
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("no coś nie smiga");
                }
            }
            Console.ReadKey();
        }

        // INICJALIZACJA REPOZYTORIUM
        public static void InitialRepositories(out DriverRepository DriverRepo, out CarRepository CarRepo)
        {
            DriverRepo = new DriverRepository();
            CarRepo = new CarRepository();
        }

        // FUNKCJA KTORA ZWRACA TYLKO LICZBY CALKOWITE
        public static int GetCorrectId(string message)
        {
            string input;
            int output;
            Console.WriteLine(message);
            input = Console.ReadLine();
            if (int.TryParse(input, out output) == true)
            {
                return output;
            }
            else
                Console.WriteLine("Id has to be a number");
            return 0;

        }

        // SZUKANIE KIEROWCY
        public static Driver GetDriverById(int id, DriverRepository repo)
        {
            Driver result;

            using (new NHibernateSessionContext())
            {
                result = repo.Find(id);
                repo.Flush();
            }

            return result;
        }


        // ZMIANA DANYCH
        public static void UpdateDriverNameAndSurnameById(int id, string newName, string newSurname, DriverRepository repo)
        {
            Driver result;

            using (new NHibernateSessionContext())
            {
                result = repo.Find(id);
                result.Name = newName;
                result.Surname = newSurname;
                repo.Save(result);
                repo.Flush();
            }
        }


        // TWORZENIE AUTA
        public static void CreateCar(string name, CarRepository repo)
        {

            var car = new Car
            {
                ModelName = name

            };

            using (new NHibernateSessionContext())
            {
                repo.Save(car);
                repo.Flush();
            }
        }

        // TWORZENIE Kierowcy
        public static void CreateDriver(string name, string surname, List<Car> carlist, DriverRepository repo)
        {

            var driver = new Driver
            {
                Name = name,
                Surname = surname,
                Cars = carlist

            };

            using (new NHibernateSessionContext())
            {
                repo.Save(driver);
                repo.Flush();
            }
        }

        // PRZYPISANIE OWNERA
        public static void UpdateCar(int CarId, int OwnerId, CarRepository repo)
        {
            Car result;

            using (new NHibernateSessionContext())
            {
                result = repo.Find(CarId);
                if (result == null)
                {
                    Console.WriteLine("Such car does not exist!");
                    return;
                }
                //          result.OwnerId = OwnerId;
                repo.Save(result);
                repo.Flush();
            }
        }


        //ZWRACANIE WYNIKOW
        public static List<Car> FindDriversCar(int Id, CarRepository repo)
        {
            List<Car> driverFromDb = new List<Car>();
            using (new NHibernateSessionContext())
            {
                // driverFromDb = repo.FindAll().Where(x => x.OwnerId == Id).ToList<Car>();
                //  IList<Car> list = repo.Session.QueryOver<Car>().Where(x => x.OwnerId == Id).List<Car>();
                //         driverFromDb = list as List<Car>;
            }

            return driverFromDb;
        }

        // WYSWIETLANIE WYNIKOW
        public static void DisplayCarList(List<Car> carList)
        {
            foreach (var x in carList)
            {
                Console.WriteLine($"Model name:  {x.ModelName}");
            }
        }

        public static void CreateTestData(int numbOfDrivers, string[] names, string[] surnames, string[] models, DriverRepository repo)
        {
            int a = 0;
            TestClass tc = new TestClass();
            while (a < numbOfDrivers)
            {
                int start = 0;
                //   Console.WriteLine(tc.ReturnRandomValue(names) + "       " + tc.ReturnRandomValue(surnames));
                string driverName = tc.ReturnRandomValue(names);
                string driverSurname = tc.ReturnRandomValue(surnames);
                List<Car> carl = new List<Car>();

                while (start < 1000)
                {
                    var car = new Car
                    {
                        ModelName = tc.ReturnRandomValue(models)

                    };

                    carl.Add(car);
                    start++;
                }

                CreateDriver(driverName, driverSurname,carl, repo);
                a++;
            }

        }
    }
}