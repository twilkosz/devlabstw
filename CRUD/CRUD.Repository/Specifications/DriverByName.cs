﻿using CRUD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volvo.NVS.Persistence.Specifications;

namespace CRUD.Repository.Specifications
{
    public class DriverByName : Specification<Driver>
    {
        public DriverByName(string name) : base(d => d.Name == name)
        { }
    }
}
