﻿using CRUD.Model;
using Volvo.NVS.Persistence.NHibernate.Repositories;

namespace CRUD.Repository
{
    public class CarRepository : GenericRepository<Car, int>
    {
    }
}
