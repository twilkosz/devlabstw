﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciLibrary
{
    public class FibEx
    {
        private void GetFibNumberHelp(int n)
        {
            if (n <= 1)
                throw new FibonaciResultException(1);
            else
            {
                int elemOne = 0, elemTwo = 0, result = 0;

                try { GetFibNumberHelp(n - 1); }
                catch (FibonaciResultException ex)
                {
                    elemOne = ex.value;
                }

                try { GetFibNumberHelp(n - 2); }
                catch (FibonaciResultException ex)
                {
                    elemTwo = ex.value;
                }
                result = elemOne + elemTwo;

                throw new FibonaciResultException(result);
            }
        }


        public int GetFibNumber(int n)
        {

            int value = 0;

            try
            {
                GetFibNumberHelp(n);
            }
            catch (FibonaciResultException ex)
            {
                value = ex.value;
            }
            return value;
        }
    }

    internal class FibonaciResultException : Exception
    {
        public int value { get; private set; }

        public FibonaciResultException(int value)
        {
            this.value = value;
        }
    }
}
