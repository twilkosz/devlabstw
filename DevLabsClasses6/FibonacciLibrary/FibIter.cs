﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciLibrary
{
    public class FibIter
    {
        public int GetFibNumber(int n)
        {
            int fibValue = 1, previous = 1, previousTwo = 0;

            for (int i = 0; i < n; i++)
            {
                fibValue = previous + previousTwo;
                previousTwo = previous;
                previous = fibValue;
            }
            return fibValue;
        }
    }
}
