﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciLibrary
{
    public class FibRec
    {
        public int GetFibNumber(int n)
        {
            if (n <= 1)
                return 1;
            else
                return GetFibNumber(n - 1) + GetFibNumber(n - 2);
        }
    }
}
