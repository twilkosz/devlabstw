﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FibonacciLibrary
{
    public static class Fibonacci
    {
        public static int GetFibNumber(int n)
        {
            int fibValue=1, previous = 1, previousTwo=0;

            for (int i = 1; i < n; i++)
            {
                fibValue = previous + previousTwo;
                previousTwo = previous;
                previous = fibValue;
            }
            return fibValue;
        }
        public static int GetFibNumberRec(int n)
        {
            if (n <= 1)
                return 1;
            else
                return GetFibNumberRec(n - 1) + GetFibNumberRec(n - 2);
        }


    }
}
